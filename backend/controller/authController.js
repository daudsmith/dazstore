'use strict';
const response = require('../config/res');
const connection = require('../config/conn');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

exports.loginUsers = async (req, res) => {
    try {
        const postData = req.body;
        const user = {
            "user_email": postData.user_email,
            "user_password": postData.user_password
        }
        connection.query('SELECT * FROM tb_users WHERE user_email = ?', async (error, results) => {
            const token = jwt.sign(user, process.env.JWT_SECRET, { expiresIn: process.env.JWT_EXPIRED_IN})
            const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN, { expiresIn: process.env.REFRESH_TOKEN})
            const cookieOptions = {expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES * 24 * 60 * 60 * 1000),httpOnly: true}
            const response = {
                "status": "login Succes",
                "token": token,
                "refreshToken": refreshToken,
            }
            res.cookie('jwt', token, cookieOptions);
            res.status(200).json(response);
        })
    } catch (error) {
        console.log(error);
    }
}

exports.registerUsers = function(req, res) {
    const {user_name, user_email, user_password} = req.body;

    connection.query('SELECT user_email from tb_users WHERE user_email = ?',
    async function () {
        const hashedPassword = await bcrypt.hash(user_password, 8);
            connection.query(' INSERT INTO tb_users SET ?', {
                user_name: user_name,
                user_email: user_email,
                user_password: hashedPassword
            }, (error) => {
                if (error) {
                    console.log(error);
                } else {
                    response.ok("Succes", res)
                }
            })
        }
    );
};