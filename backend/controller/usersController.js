'use strict';
const response = require('../config/res');
const connection = require('../config/conn');

exports.users = function(req, res) {
    connection.query('SELECT * FROM tb_users', function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok(rows, res)
        }
    });
};

exports.findUsers = function(req, res) {
    
    const user_id = req.params.user_id;

    connection.query('SELECT * FROM tb_users where user_id = ?',
    [ user_id ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok(rows, res)
        }
    });
};

exports.createUsers = function(req, res) {
    
    const user_name = req.body.user_name;
    const user_email = req.body.user_email;
    const user_password = req.body.user_password;

    connection.query('INSERT INTO tb_users (user_name, user_email, user_password) values (?,?,?,?)',
    [ user_name, user_email, user_password ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("Succes", res)
        }
    });
};

exports.updateUsers = function(req, res) {

    const user_id = req.body.user_id;
    const user_name = req.body.user_name;
    const user_email = req.body.user_email;
    const user_password = req.body.user_password;

    connection.query('UPDATE tb_users SET user_name = ?, user_email = ?, user_password = ? WHERE user_id = ?',
    [ user_name, user_email, user_password, user_id ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("Succes", res)
        }
    });
};

exports.deleteUsers = function(req, res) {
    
    const user_id = req.body.user_id;

    connection.query('DELETE FROM tb_users WHERE user_id = ?',
    [ user_id ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("Succes", res)
        }
    });
};