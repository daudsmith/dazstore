const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const indexRoutes = require("../routes/indexRoutes")
const userRoutes = require("../routes/usersRoutes");
const authRoutes = require('../routes/authRoutes');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.json());
app.use(cookieParser());

indexRoutes(app);
userRoutes(app);
authRoutes(app);

module.exports = app;