const http = require('http');
const app = require('./config/app');
const dotenv = require("dotenv");
dotenv.config({ path: './.env'});

const port = process.env.PORT_ACCES;
const server = http.createServer(app);

server.listen(port);
console.log('server started on port: ' + port);