'use strict';

module.exports = function(app) {
    const authController = require('../controller/authController');

    app.route('/api/login')
        .post(authController.loginUsers);

    app.route('/api/register')
        .post(authController.registerUsers);
};