'use strict';

module.exports = function(app) {
    const indexController = require('../controller/indexController');

    app.route('/')
        .get(indexController.index);
};