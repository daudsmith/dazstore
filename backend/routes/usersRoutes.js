'use strict';

module.exports = function(app) {
    var userController = require('../controller/usersController');

    app.route('/api/getall/users')
        .get(userController.users);

    app.route('/api/find/users/:id_user')
        .get(userController.findUsers);

    app.route('/api/create/users')
        .post(userController.createUsers);

    app.route('/api/update/users')
        .put(userController.updateUsers);
    
    app.route('/api/delete/users')
        .delete(userController.deleteUsers);
};